#ifndef DELETEWORD_H
#define DELETEWORD_H

#include <QDialog>
#include <QLineEdit>
#include <QPushButton>
#include <QGridLayout>
#include <QLabel>
#include "database.h"

namespace Ui {
class DeleteWord;
}

class DeleteWord : public QDialog
{
    Q_OBJECT
    
public:
    explicit DeleteWord(QWidget *parent = 0);
    virtual ~DeleteWord();
    
private:
    Ui::DeleteWord *ui;
    Database *db;
    QGridLayout *layout;
    QLineEdit *le_word;
    QPushButton *btn_add;
    QLabel *lbl_result;

private slots:
    void delete_clicked();
};

#endif // DELETEWORD_H
