#ifndef DATABASE_H
#define DATABASE_H

#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlRecord>
#include <QVariant>
#include <QSqlError>
#include <QDir>
#include <QStringList>

class Database
{
public:
    Database();
    ~Database();
    bool open();
    void close();

    bool addWord(QString word, QString &result);
    bool deleteWord(QString word);
    QString get_word();
    int get_size();

private:
    QSqlDatabase db;
    QString word;
};

#endif // DATABASE_H
