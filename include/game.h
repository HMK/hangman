#ifndef GAME_H
#define GAME_H
#include <QString>
#include "database.h"
#include <QVector>

class Game
{
public:
    Game();
    ~Game();

    QString new_word();
    QString get_word();
    QVector<int> has_char(QChar _char);
    QString replace(QVector<int> indices, QChar _char);
    QString get_us_word();

private:
    QString word;
    QString us_word;
    Database *db;
};

#endif // GAME_H
