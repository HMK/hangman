#ifndef ADDWORD_H
#define ADDWORD_H

#include <QDialog>
#include <QLineEdit>
#include <QPushButton>
#include <QGridLayout>
#include <QLabel>
#include "database.h"

namespace Ui {
class AddWord;
}

class AddWord : public QDialog
{
    Q_OBJECT
    
public:
    explicit AddWord(QWidget *parent = 0);
    virtual ~AddWord();
    
private:
    Ui::AddWord *ui;
    Database *db;
    QGridLayout *layout;
    QLineEdit *le_word;
    QPushButton *btn_add;
    QLabel *lbl_result;

private slots:
    void add_clicked();
};

#endif // ADDWORD_H
