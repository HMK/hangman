#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QString>
#include "../include/game.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    
private slots:
    void on_actionAdd_word_triggered();
    void on_actionDelete_word_triggered();
    void on_btn_a_clicked();
    void on_btn_b_clicked();
    void on_btn_c_clicked();
    void on_btn_d_clicked();
    void on_btn_e_clicked();
    void on_btn_f_clicked();
    void on_btn_g_clicked();
    void on_btn_h_clicked();
    void on_btn_i_clicked();
    void on_bn_j_clicked();
    void on_btn_k_clicked();
    void on_btn_l_clicked();
    void on_btn_m_clicked();
    void on_btn_n_clicked();
    void on_btn_o_clicked();
    void on_btn_p_clicked();
    void on_btn_q_clicked();
    void on_btn_r_clicked();
    void on_btn_s_clicked();
    void on_btn_t_clicked();
    void on_btn_u_clicked();
    void on_btn_v_clicked();
    void on_btn_w_clicked();
    void on_btn_x_clicked();
    void on_btn_y_clicked();
    void on_btn_z_clicked();

    void on_actionNew_triggered();

private:
    Ui::MainWindow *ui;
    QString version;
    Game *game;
    static int max_wrong_count;
    int wrong_count;
    int total_score;

    void all_buttons_set_enabled(bool b);
    void check_game(QChar _char);
    void new_game();
    bool win_game();
};

#endif // MAINWINDOW_H
