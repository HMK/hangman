#include "../include/addword.h"
#include "ui_addword.h"

AddWord::AddWord(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AddWord)
{
    ui->setupUi(this);

    db = new Database();

    layout = new QGridLayout();
    le_word = new QLineEdit();
    btn_add = new QPushButton(tr("Add"));
    lbl_result = new QLabel();

    layout->addWidget(le_word, 0, 0, 0, 0);
    layout->addWidget(btn_add, 1, 0, 1, 1, Qt::AlignLeft);
    layout->addWidget(lbl_result, 1, 1);
    setLayout(layout);
    connect(btn_add, SIGNAL(clicked()), this, SLOT(add_clicked()));
}

AddWord::~AddWord()
{
    delete db;
    delete layout;
    delete le_word;
    delete btn_add;
    delete lbl_result;
    delete ui;
}

void AddWord::add_clicked(){
    db->open();
    QString result;
    db->addWord(le_word->text().trimmed(), result);
    lbl_result->setText("\'" + le_word->text() + "\'" + result);
    db->close();
}
