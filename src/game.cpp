#include "../include/game.h"

Game::Game()
{
    db = new Database();
}

Game::~Game(){
    db->close();
    delete db;
}

QString Game::new_word(){
    if(db->open()){
        word = db->get_word();
        word = word.toUpper();
        us_word = "";
        us_word = us_word.fill('_', word.size());
        db->close();
        return word;
    }
    return "";
    db->close();
}

QString Game::get_word(){
    return word;
}

QVector<int> Game::has_char(QChar _char){
    QVector<int> indices;
    for(int i = 0; i < word.size(); i++){
        if(word.at(i) == _char){
            indices.push_back(i);
        }
    }
    return indices;
}

QString Game::replace(QVector<int> indices, QChar _char){

    for(int i = 0; i < indices.size(); i++){
        us_word.replace(indices[i], 1, _char);
    }
    return us_word;
}

QString Game::get_us_word(){
    return us_word;
}
