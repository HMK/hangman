#include "../include/database.h"
#include <cstdlib>
#include <ctime>

Database::Database()
{
    db = QSqlDatabase::addDatabase("QSQLITE");
    srand(time(0));
}

Database::~Database(){
    db.close();
}

bool Database::open(){
#ifdef __MINGW32__
    QString path(getenv("ProgramFiles"));
    home_path += "\\Hangman\\";
#elif __GNUC__
    QDir *dir = new QDir();
    QString path = dir->currentPath();
#endif
    QString db_name(path + "/database/words.sqlite");

    db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName(db_name);
    bool status = db.open();
    return status;
}

void Database::close(){
    db.close();
}

bool Database::addWord(QString word, QString &result){
    if(!word.isEmpty()){
        QSqlQuery query;
        query.prepare("INSERT INTO table_words VALUES (?)");
        query.addBindValue(QVariant(word));
        bool status = query.exec();

        if(!query.lastError().databaseText().isNull()){
            if(query.lastError().databaseText() == "column word is not unique")
                result = " is already in the database. Try another one.";
            else
                result = query.lastError().databaseText();
            db.close();
            return status;
        }
        else{
            result = " added to database.";
            db.close();
            return status;
        }
    }
    db.close();
    return false;
}

bool Database::deleteWord(QString word){
    if(!word.isEmpty()){
        QSqlQuery query;
        query.prepare("DELETE FROM table_words WHERE word=?");
        query.addBindValue(word);
        bool status = query.exec();
        if(query.numRowsAffected() == 0){
            db.close();
            return false;
        }
        return status;
    }
    db.close();
    return false;
}

QString Database::get_word(){
    int random = rand() % get_size();
    if(db.open()){
        QSqlQuery query;
        query.prepare("SELECT * FROM table_words");
        query.exec();
        int count = 0;
        while(query.next()){
            if(count == random){
                return query.value("word").toString();
            }
            count++;
        }
        return "";
    }
    return "";
}

int Database::get_size(){
    if(db.open()){
        QSqlQuery query("SELECT * FROM table_words");
        int size = 0;
        while(query.next()){
            size++;
        }
        db.close();
        return size;
    }
    return -1;
}
