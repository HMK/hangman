#include "../include/deleteword.h"
#include "ui_deleteword.h"

DeleteWord::DeleteWord(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DeleteWord)
{
    ui->setupUi(this);

    db = new Database();

    layout = new QGridLayout();
    le_word = new QLineEdit();
    btn_add = new QPushButton(tr("Delete"));
    lbl_result = new QLabel();

    layout->addWidget(le_word, 0, 0, 0, 0);
    layout->addWidget(btn_add, 1, 0, 1, 1, Qt::AlignLeft);
    layout->addWidget(lbl_result, 1, 1);
    setLayout(layout);
    connect(btn_add, SIGNAL(clicked()), this, SLOT(delete_clicked()));
}

DeleteWord::~DeleteWord()
{
    delete db;
    delete layout;
    delete le_word;
    delete btn_add;
    delete lbl_result;
    delete ui;
}

void DeleteWord::delete_clicked(){
    db->open();
    QString result;
    if(db->deleteWord(le_word->text().trimmed())){
        lbl_result->setText("\'" + le_word->text()+ "\'" + " is deleted.");
    }
    else{
        lbl_result->setText("\'" + le_word->text()+ "\'" + " is not exist in the database.");
    }

    db->close();
}
