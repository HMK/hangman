#include "ui_mainwindow.h"
#include "../include/mainwindow.h"
#include "../include/addword.h"
#include "../include/deleteword.h"
#include <QDir>
#include <QMessageBox>
#include <QFont>

int MainWindow::max_wrong_count = 5;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    version = "1.0";
    setWindowTitle("Hangman v" + version);
    total_score = 0;
    wrong_count = 0;
    game = new Game();
    ui->lbl_score->setText("Total score: " + QString::number(total_score));
    ui->lbl_wrong_count->setText("Wrong: " + QString::number(wrong_count) + "/5" + QString::number(max_wrong_count));

#ifdef __MINGW32__
    QFont font("Arial");
    font.setPointSize(26);
    font.setItalic(true);
    font.setLetterSpacing(QFont::AbsoluteSpacing, 8);
#elif __GNUC__
    QFont font("Purisa");
    font.setPointSize(26);
    font.setBold(true);
    font.setItalic(true);
    font.setLetterSpacing(QFont::AbsoluteSpacing, 8);
#endif
    ui->lbl_word->setFont(font);

    new_game();
}

MainWindow::~MainWindow()
{
    delete game;
    delete ui;
}

void MainWindow::check_game(QChar _char){

    QVector<int> vi = game->has_char(_char);
    if(vi.size()){
        QString _word = game->replace(vi, _char);
        ui->lbl_word->setText(_word);
        if(win_game()){
            all_buttons_set_enabled(true);
            _char = ' ';
            wrong_count = 0;
            ui->lbl_wrong_count->setText("Wrong: " + QString::number(wrong_count) + "/" + QString::number(max_wrong_count));
            game->new_word();
            ui->lbl_word->setText(game->get_us_word());
            wrong_count = 0;
            all_buttons_set_enabled(true);
        }
    }
    else{
        wrong_count++;
        ui->lbl_wrong_count->setText("Wrong: " + QString::number(wrong_count) + "/" + QString::number(max_wrong_count));
        if(wrong_count == max_wrong_count){
            QMessageBox msg;
            msg.setWindowTitle(tr("Fail"));
            msg.setText(tr("You couldn't win :("));
            msg.exec();

            all_buttons_set_enabled(false);
            ui->lbl_word->setText(game->get_word());
        }
    }

    switch (_char.toLatin1()) {
    case 'A':
        ui->btn_a->setEnabled(false);
        break;
    case 'B':
        ui->btn_b->setEnabled(false);
        break;
    case 'C':
        ui->btn_c->setEnabled(false);
        break;
    case 'D':
        ui->btn_d->setEnabled(false);
        break;
    case 'E':
        ui->btn_e->setEnabled(false);
        break;
    case 'F':
        ui->btn_f->setEnabled(false);
        break;
    case 'G':
        ui->btn_g->setEnabled(false);
        break;
    case 'H':
        ui->btn_h->setEnabled(false);
        break;
    case 'I':
        ui->btn_i->setEnabled(false);
        break;
    case 'J':
        ui->bn_j->setEnabled(false);
        break;
    case 'K':
        ui->btn_k->setEnabled(false);
        break;
    case 'L':
        ui->btn_l->setEnabled(false);
        break;
    case 'M':
        ui->btn_m->setEnabled(false);
        break;
    case 'N':
        ui->btn_n->setEnabled(false);
        break;
    case 'O':
        ui->btn_o->setEnabled(false);
        break;
    case 'P':
        ui->btn_p->setEnabled(false);
        break;
    case 'Q':
        ui->btn_q->setEnabled(false);
        break;
    case 'R':
        ui->btn_r->setEnabled(false);
        break;
    case 'S':
        ui->btn_s->setEnabled(false);
        break;
    case 'T':
        ui->btn_t->setEnabled(false);
        break;
    case 'U':
        ui->btn_u->setEnabled(false);
        break;
    case 'V':
        ui->btn_v->setEnabled(false);
        break;
    case 'W':
        ui->btn_w->setEnabled(false);
        break;
    case 'X':
        ui->btn_x->setEnabled(false);
        break;
    case 'Y':
        ui->btn_y->setEnabled(false);
        break;
    case 'Z':
        ui->btn_z->setEnabled(false);
        break;
    default:
        break;
    }
}

void MainWindow::new_game(){
    game->new_word();
    ui->lbl_word->setText(game->get_us_word());
    wrong_count = 0;
    all_buttons_set_enabled(true);
    total_score = 0;
    ui->lbl_score->setText("Total score: " + QString::number(total_score));
    ui->lbl_wrong_count->setText("Wrong: " + QString::number(wrong_count));
}

bool MainWindow::win_game(){
    if(!game->get_us_word().contains('_')){
        ui->lbl_word->setText(game->get_word());
        all_buttons_set_enabled(false);
        QMessageBox msg;
        msg.setWindowTitle("Congratulation!");
        msg.setText("You win!");
        msg.addButton(QMessageBox::Ok);
        msg.addButton(QMessageBox::Abort);
        msg.setDefaultButton(QMessageBox::Ok);
        msg.setButtonText(QMessageBox::Ok, "Next");
        msg.setButtonText(QMessageBox::Abort, "Quit");
        if(msg.exec() == QMessageBox::Ok){
            total_score += 20;
            ui->lbl_score->setText("Total score: " + QString::number(total_score));
            return true;
        }
        else{
            exit(0);
        }
    }
    return false;
}

void MainWindow::all_buttons_set_enabled(bool b){
    ui->btn_a->setEnabled(b);
    ui->btn_b->setEnabled(b);
    ui->btn_c->setEnabled(b);
    ui->btn_d->setEnabled(b);
    ui->btn_e->setEnabled(b);
    ui->btn_f->setEnabled(b);
    ui->btn_g->setEnabled(b);
    ui->btn_h->setEnabled(b);
    ui->btn_i->setEnabled(b);
    ui->bn_j->setEnabled(b);
    ui->btn_k->setEnabled(b);
    ui->btn_l->setEnabled(b);
    ui->btn_m->setEnabled(b);
    ui->btn_n->setEnabled(b);
    ui->btn_o->setEnabled(b);
    ui->btn_p->setEnabled(b);
    ui->btn_q->setEnabled(b);
    ui->btn_r->setEnabled(b);
    ui->btn_s->setEnabled(b);
    ui->btn_t->setEnabled(b);
    ui->btn_u->setEnabled(b);
    ui->btn_v->setEnabled(b);
    ui->btn_w->setEnabled(b);
    ui->btn_x->setEnabled(b);
    ui->btn_y->setEnabled(b);
    ui->btn_z->setEnabled(b);
}

void MainWindow::on_actionAdd_word_triggered()
{
    AddWord *add_word = new AddWord;
    add_word->show();
}

void MainWindow::on_actionDelete_word_triggered()
{
    DeleteWord *delete_word = new DeleteWord;
    delete_word->show();
}



void MainWindow::on_btn_a_clicked()
{
    check_game('A');
}

void MainWindow::on_btn_b_clicked()
{
    check_game('B');
}

void MainWindow::on_btn_c_clicked()
{
    check_game('C');
}

void MainWindow::on_btn_d_clicked()
{
    check_game('D');
}

void MainWindow::on_btn_e_clicked()
{
    check_game('E');
}

void MainWindow::on_btn_f_clicked()
{
    check_game('F');
}

void MainWindow::on_btn_g_clicked()
{
    check_game('G');
}

void MainWindow::on_btn_h_clicked()
{
    check_game('H');
}

void MainWindow::on_btn_i_clicked()
{
    check_game('I');
}

void MainWindow::on_bn_j_clicked()
{
    check_game('J');
}

void MainWindow::on_btn_k_clicked()
{
    check_game('K');
}

void MainWindow::on_btn_l_clicked()
{
    check_game('L');
}

void MainWindow::on_btn_m_clicked()
{
    check_game('M');
}

void MainWindow::on_btn_n_clicked()
{
    check_game('N');
}

void MainWindow::on_btn_o_clicked()
{
    check_game('O');
}

void MainWindow::on_btn_p_clicked()
{
    check_game('P');
}

void MainWindow::on_btn_q_clicked()
{
    check_game('Q');
}

void MainWindow::on_btn_r_clicked()
{
    check_game('R');
}

void MainWindow::on_btn_s_clicked()
{
    check_game('S');
}

void MainWindow::on_btn_t_clicked()
{
    check_game('T');
}

void MainWindow::on_btn_u_clicked()
{
    check_game('U');
}

void MainWindow::on_btn_v_clicked()
{
    check_game('V');
}

void MainWindow::on_btn_w_clicked()
{
    check_game('W');
}

void MainWindow::on_btn_x_clicked()
{
    check_game('X');
}

void MainWindow::on_btn_y_clicked()
{
    check_game('Y');
}

void MainWindow::on_btn_z_clicked()
{
    check_game('Z');
}

void MainWindow::on_actionNew_triggered()
{
    new_game();
}

